package Jizzy;
use Mojo::Base 'Mojolicious';
use Jizzy::Schema;
use Mojolicious::Static;

# Connects once for entire application. For real apps, consider using a helper
# that can reconnect on each request if necessary.
#has schema => sub {
#  return Jizzy::Schema->connect('dbi:SQLite:' . ($ENV{TEST_DB} || 'db.sqlite3'));
#};

# This method will run once at server start
sub startup {
  my $self = shift;

  # Helper must inside sub startup function?
  # Yes, or you can move it into a separate file and load helpers in from there
  # https://github.com/mojolicious/mojo/wiki/Working-with-helpers
  # Thanks I read this URL after your lesson ;)
  $self->helper(select_options_from_resultset => sub {  #I must copy all helper to other records ? not sure what you mean
    my ($self, $resultset, $selected_id) = @_;          #I have more select_field in project and not only Category
                                          # We can make this more generic
                                          # Now you can call it with the resultset name and an id to mark as selected
                                          # % my $options = select_options_from_resultset 'Category' => 3
    #my $items = $self->app->db->resultset($resultset);
    my $items = $self->db->resultset($resultset);

    my $options = Mojo::Collection->new(['*' => 0]);
    while (my $item = $items->next) {
      my @attr;
      push @attr, selected => 'selected' if $item->id == $selected_id;
      if($resultset eq "Offer"){
        push @{$options}, [$item->title, $item->id, @attr];
      } else {
        push @{$options}, [$item->name, $item->id, @attr];
      }
    }

    return $options;
  });

  # DSN syntax
  # dbi:DriverName:database=database_name;host=hostname;port=port
  # For SQLite:
  my $schema = Jizzy::Schema->connect('dbi:SQLite:db.sqlite3', '', '', { sqlite_unicode => 1});
  #my $schema = Jizzy::Schema->connect('dbi:SQLite:/home/MushiX/domains/mojo.mushix.usermd.net/public_html/db.sqlite3', '', '', { sqlite_unicode => 1});

  #$self->helper(db => sub { $self->app->schema });

  $self->helper(db => sub { return $schema; });

  # Load configuration from hash returned by "my_app.conf"
  #my $config = $self->plugin('Config');

  # Documentation browser under "/perldoc"
  #$self->plugin('PODRenderer') if $config->{perldoc};

  # Router
  my $r = $self->routes;

  # Normal route to controller

  #Public
  $r->get('/')->to('public#index');

  #Users
  $r->get('/users')->to('users#index');
  #$r->get('/users/:id')->to('users#create');
  #$authorized->get('/create')->name('create_post')->to(template => 'user/create_user');
  #$authorized->post('/create')->name('creating_post')->to('users#create');
  $r->get('/users/store')->name('store_user')->to('users#store');
  $r->post('/users/create')->name('create_user')->to('users#create');
  $r->get('/users/show/:id')->name('show_user')->to('users#show');
  $r->get('/users/edit/:id')->name('edit_user')->to('users#edit');
  $r->post('/users/update/:id')->name('update_user')->to('users#update');
  $r->post('/users/remove/:id', [id => qr/\d+/])->name('remove_user')->to('users#remove');

  #Offers
  $r->get('/offers')->to('offers#index');
  $r->get('/offers/store')->name('store_offer')->to('offers#store');
  $r->post('/offers/create')->name('create_offer')->to('offers#create');
  $r->get('/offers/show/:id')->name('show_offer')->to('offers#show');
  $r->get('/offers/edit/:id')->name('edit_offer')->to('offers#edit');
  $r->post('/offers/update/:id')->name('update_offer')->to('offers#update');
  $r->post('/offers/remove/:id', [id => qr/\d+/])->name('remove_offer')->to('offers#remove');

  #photos
  $r->get('/photos')->to('photos#index');
  $r->get('/photos/store')->name('store_photo')->to('photos#store');
  $r->post('/photos/create')->name('create_photo')->to('photos#create');
  $r->get('/photos/show/:id')->name('show_photo')->to('photos#show');
  $r->get('/photos/edit/:id')->name('edit_photo')->to('photos#edit');
  $r->post('/photos/update/:id')->name('update_photo')->to('photos#update');
  $r->post('/photos/remove/:id', [id => qr/\d+/])->name('remove_photo')->to('photos#remove');

  #Cities
=begin
  my $r_cities = $r->get('/cities')->to(controller => 'cities');
  $r_cities->get('/')->to(action => 'index');
  $r_cities->get('/new')->name('new_city')->to(template => 'cities/new');
  $r_cities->post('/create')->name('create_city')->to('cities#create');
  $r_cities->get('/show/:id')->name('show_city')->to(action => 'show');
  $r_cities->get('/edit/:id')->name('edit_city')->to(action => 'edit');
  $r_cities->post('/update')->name('update_city')->to('cities#update');
  $r_cities->post('/delete/:id', [id => qr/\d+/])->name('delete_city')->to('cities#delete');
=cut
  $r->get('/cities')->to('cities#index');
  $r->get('/cities/store')->name('store_city')->to('cities#store');
  $r->post('/cities/create')->name('create_city')->to('cities#create');
  $r->get('/cities/show/:id')->name('show_city')->to('cities#show');
  $r->get('/cities/edit/:id')->name('edit_city')->to('cities#edit');
  $r->post('/cities/update/:id')->name('update_city')->to('cities#update');
  $r->post('/cities/remove/:id', [id => qr/\d+/])->name('remove_city')->to('cities#remove');

  #Yes I now see, but cities/new != cities#new ? what different?
  #Categories
  $r->get('/categories')->to('categories#index');
   # this doesn't use the 'new' action in the controller! it just renders the template
   # ->to('controller#action') # uses the action in the controller
   # ->to(template => 'controller/action') # uses the template "action" in the "controller" directory in templates
   # template in the controller ?
   # Yeah, usually the templates are put in directories named after the controller that uses them
   # In lib/Jizzy/Controller/_template.html.ep ?
   # Don't worry about it :P / Ok all at front for me(all ahead of me)
   # I have problem with Categories.pm in "sub new {}"
  $r->get('/categories/store')->name('store_category')->to('categories#store'); #(template => 'categories/new');
  $r->post('/categories/create')->name('create_category')->to('categories#create');
  $r->get('/categories/show/:id')->name('show_category')->to('categories#show');
  $r->get('/categories/edit/:id')->name('edit_category')->to('categories#edit');
  $r->post('/categories/update/:id')->name('update_category')->to('categories#update');
  $r->post('/categories/remove/:id', [id => qr/\d+/])->name('remove_category')->to('categories#remove');
}

1;
