use utf8;
package Jizzy::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-17 14:23:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Asq9toBi1J7w7HBuLrLGSA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;