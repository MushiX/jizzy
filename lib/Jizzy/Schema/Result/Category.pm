use utf8;
package Jizzy::Schema::Result::Category;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Jizzy::Schema::Result::Category

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<Categories>

=cut

__PACKAGE__->table("Categories");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 position

  data_type: 'integer'
  is_nullable: 0

=head2 parent_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "position",
  { data_type => "integer", is_nullable => 0 },
  "parent_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 categories

Type: has_many

Related object: L<Jizzy::Schema::Result::Category>

=cut

__PACKAGE__->has_many(
  "categories",
  "Jizzy::Schema::Result::Category",
  { "foreign.parent_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 offers

Type: has_many

Related object: L<Jizzy::Schema::Result::Offer>

=cut

__PACKAGE__->has_many(
  "offers",
  "Jizzy::Schema::Result::Offer",
  { "foreign.category_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 parent

Type: belongs_to

Related object: L<Jizzy::Schema::Result::Category>

=cut

__PACKAGE__->belongs_to(
  "parent",
  "Jizzy::Schema::Result::Category",
  { id => "parent_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-11-05 11:03:58
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:w1cBh6UZ3XwVoD6M5hIJMg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
