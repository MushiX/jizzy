use utf8;
package Jizzy::Schema::Result::User;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Jizzy::Schema::Result::User

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<Users>

=cut

__PACKAGE__->table("Users");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 surname

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 address

  data_type: 'varchar'
  is_nullable: 0
  size: 70

=head2 email

  data_type: 'varchar'
  is_nullable: 0
  size: 80

=head2 password

  data_type: 'varchar'
  is_nullable: 0
  size: 60

=head2 avatar

  data_type: 'blob'
  is_nullable: 1

=head2 lang

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 is_admin

  data_type: 'boolean'
  default_value: 'f'
  is_nullable: 1

=head2 is_active

  data_type: 'boolean'
  default_value: 'f'
  is_nullable: 1

=head2 created_at

  data_type: 'datetime'
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  is_nullable: 0

=head2 city_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "surname",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "address",
  { data_type => "varchar", is_nullable => 0, size => 70 },
  "email",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "password",
  { data_type => "varchar", is_nullable => 0, size => 60 },
  "avatar",
  { data_type => "blob", is_nullable => 1 },
  "lang",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "is_admin",
  { data_type => "boolean", default_value => "f", is_nullable => 1 },
  "is_active",
  { data_type => "boolean", default_value => "f", is_nullable => 1 },
  "created_at",
  { data_type => "datetime", is_nullable => 0 },
  "updated_at",
  { data_type => "datetime", is_nullable => 0 },
  "city_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<email_unique>

=over 4

=item * L</email>

=back

=cut

__PACKAGE__->add_unique_constraint("email_unique", ["email"]);

=head1 RELATIONS

=head2 city

Type: belongs_to

Related object: L<Jizzy::Schema::Result::City>

=cut

__PACKAGE__->belongs_to(
  "city",
  "Jizzy::Schema::Result::City",
  { id => "city_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 comments

Type: has_many

Related object: L<Jizzy::Schema::Result::Comment>

=cut

__PACKAGE__->has_many(
  "comments",
  "Jizzy::Schema::Result::Comment",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 conversations_recipients

Type: has_many

Related object: L<Jizzy::Schema::Result::Conversation>

=cut

__PACKAGE__->has_many(
  "conversations_recipients",
  "Jizzy::Schema::Result::Conversation",
  { "foreign.recipient_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 conversations_senders

Type: has_many

Related object: L<Jizzy::Schema::Result::Conversation>

=cut

__PACKAGE__->has_many(
  "conversations_senders",
  "Jizzy::Schema::Result::Conversation",
  { "foreign.sender_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 likes

Type: has_many

Related object: L<Jizzy::Schema::Result::Like>

=cut

__PACKAGE__->has_many(
  "likes",
  "Jizzy::Schema::Result::Like",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 messages

Type: has_many

Related object: L<Jizzy::Schema::Result::Message>

=cut

__PACKAGE__->has_many(
  "messages",
  "Jizzy::Schema::Result::Message",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 offers

Type: has_many

Related object: L<Jizzy::Schema::Result::Offer>

=cut

__PACKAGE__->has_many(
  "offers",
  "Jizzy::Schema::Result::Offer",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-11-05 10:17:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yYxq9mQ7mbfkKlXXNhZ6wQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
