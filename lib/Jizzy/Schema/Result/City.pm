use utf8;
package Jizzy::Schema::Result::City;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Jizzy::Schema::Result::City

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<Cities>

=cut

__PACKAGE__->table("Cities");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 zip_code

  data_type: 'varchar'
  is_nullable: 0
  size: 6

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "zip_code",
  { data_type => "varchar", is_nullable => 0, size => 6 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 offers

Type: has_many

Related object: L<Jizzy::Schema::Result::Offer>

=cut

__PACKAGE__->has_many(
  "offers",
  "Jizzy::Schema::Result::Offer",
  { "foreign.city_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 users

Type: has_many

Related object: L<Jizzy::Schema::Result::User>

=cut

__PACKAGE__->has_many(
  "users",
  "Jizzy::Schema::Result::User",
  { "foreign.city_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 16:53:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:8pN+PaJzr8H2MFFMowQVSw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
