use utf8;
package Jizzy::Schema::Result::Comment;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Jizzy::Schema::Result::Comment

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<Comments>

=cut

__PACKAGE__->table("Comments");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 content

  data_type: 'text'
  is_nullable: 0

=head2 is_complaint

  data_type: 'boolean'
  default_value: 'f'
  is_nullable: 1

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 offer_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "content",
  { data_type => "text", is_nullable => 0 },
  "is_complaint",
  { data_type => "boolean", default_value => "f", is_nullable => 1 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "offer_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 offer

Type: belongs_to

Related object: L<Jizzy::Schema::Result::Offer>

=cut

__PACKAGE__->belongs_to(
  "offer",
  "Jizzy::Schema::Result::Offer",
  { id => "offer_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 user

Type: belongs_to

Related object: L<Jizzy::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "Jizzy::Schema::Result::User",
  { id => "user_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 15:49:48
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:rJVwM17LwskUemuGYVpNpg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
