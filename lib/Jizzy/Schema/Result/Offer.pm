use utf8;
package Jizzy::Schema::Result::Offer;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Jizzy::Schema::Result::Offer

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<Offers>

=cut

__PACKAGE__->table("Offers");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 content

  data_type: 'text'
  is_nullable: 0

=head2 price

  data_type: 'float'
  is_nullable: 0

=head2 visible

  data_type: 'boolean'
  default_value: 't'
  is_nullable: 1

=head2 end_at

  data_type: 'datetime'
  is_nullable: 0

=head2 created_at

  data_type: 'datetime'
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  is_nullable: 0

=head2 category_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 city_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "title",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "content",
  { data_type => "text", is_nullable => 0 },
  "price",
  { data_type => "float", is_nullable => 0 },
  "visible",
  { data_type => "boolean", default_value => "t", is_nullable => 1 },
  "end_at",
  { data_type => "datetime", is_nullable => 0 },
  "created_at",
  { data_type => "datetime", is_nullable => 0 },
  "updated_at",
  { data_type => "datetime", is_nullable => 0 },
  "category_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "city_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 category

Type: belongs_to

Related object: L<Jizzy::Schema::Result::Category>

=cut

__PACKAGE__->belongs_to(
  "category",
  "Jizzy::Schema::Result::Category",
  { id => "category_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 city

Type: belongs_to

Related object: L<Jizzy::Schema::Result::City>

=cut

__PACKAGE__->belongs_to(
  "city",
  "Jizzy::Schema::Result::City",
  { id => "city_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 comments

Type: has_many

Related object: L<Jizzy::Schema::Result::Comment>

=cut

__PACKAGE__->has_many(
  "comments",
  "Jizzy::Schema::Result::Comment",
  { "foreign.offer_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 conversations

Type: has_many

Related object: L<Jizzy::Schema::Result::Conversation>

=cut

__PACKAGE__->has_many(
  "conversations",
  "Jizzy::Schema::Result::Conversation",
  { "foreign.offer_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 likes

Type: has_many

Related object: L<Jizzy::Schema::Result::Like>

=cut

__PACKAGE__->has_many(
  "likes",
  "Jizzy::Schema::Result::Like",
  { "foreign.offer_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 photos

Type: has_many

Related object: L<Jizzy::Schema::Result::Photo>

=cut

__PACKAGE__->has_many(
  "photos",
  "Jizzy::Schema::Result::Photo",
  { "foreign.offer_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 promotions

Type: has_many

Related object: L<Jizzy::Schema::Result::Promotion>

=cut

__PACKAGE__->has_many(
  "promotions",
  "Jizzy::Schema::Result::Promotion",
  { "foreign.offer_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 user

Type: belongs_to

Related object: L<Jizzy::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "Jizzy::Schema::Result::User",
  { id => "user_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 16:53:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SxYohspdm6LtRJ9cFSpUQw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
