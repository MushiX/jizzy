use utf8;
package Jizzy::Schema::Result::Conversation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Jizzy::Schema::Result::Conversation

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<Conversations>

=cut

__PACKAGE__->table("Conversations");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 offer_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 sender_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 recipient_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "title",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "offer_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "sender_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "recipient_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "updated_at",
  { data_type => "datetime", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 messages

Type: has_many

Related object: L<Jizzy::Schema::Result::Message>

=cut

__PACKAGE__->has_many(
  "messages",
  "Jizzy::Schema::Result::Message",
  { "foreign.conversation_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 offer

Type: belongs_to

Related object: L<Jizzy::Schema::Result::Offer>

=cut

__PACKAGE__->belongs_to(
  "offer",
  "Jizzy::Schema::Result::Offer",
  { id => "offer_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 recipient

Type: belongs_to

Related object: L<Jizzy::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "recipient",
  "Jizzy::Schema::Result::User",
  { id => "recipient_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 sender

Type: belongs_to

Related object: L<Jizzy::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "sender",
  "Jizzy::Schema::Result::User",
  { id => "sender_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 16:53:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:0/VWtZjIqqAT/IvQS1ZfZw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
