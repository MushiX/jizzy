use utf8;
package Jizzy::Schema::Result::Promotion;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Jizzy::Schema::Result::Promotion

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<Promotions>

=cut

__PACKAGE__->table("Promotions");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 start_at

  data_type: 'datetime'
  is_nullable: 0

=head2 end_at

  data_type: 'datetime'
  is_nullable: 0

=head2 state

  data_type: 'character'
  default_value: 'n'
  is_nullable: 1
  size: 1

=head2 offer_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "start_at",
  { data_type => "datetime", is_nullable => 0 },
  "end_at",
  { data_type => "datetime", is_nullable => 0 },
  "state",
  { data_type => "character", default_value => "n", is_nullable => 1, size => 1 },
  "offer_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 offer

Type: belongs_to

Related object: L<Jizzy::Schema::Result::Offer>

=cut

__PACKAGE__->belongs_to(
  "offer",
  "Jizzy::Schema::Result::Offer",
  { id => "offer_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 16:53:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:tpNBoNAsjmInjjWh+r5Mwg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
