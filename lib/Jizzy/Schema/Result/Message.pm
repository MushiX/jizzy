use utf8;
package Jizzy::Schema::Result::Message;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Jizzy::Schema::Result::Message

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<Messages>

=cut

__PACKAGE__->table("Messages");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 content

  data_type: 'text'
  is_nullable: 0

=head2 created_at

  data_type: 'datetime'
  is_nullable: 0

=head2 view_at

  data_type: 'datetime'
  is_nullable: 1

=head2 conversation_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "content",
  { data_type => "text", is_nullable => 0 },
  "created_at",
  { data_type => "datetime", is_nullable => 0 },
  "view_at",
  { data_type => "datetime", is_nullable => 1 },
  "conversation_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 conversation

Type: belongs_to

Related object: L<Jizzy::Schema::Result::Conversation>

=cut

__PACKAGE__->belongs_to(
  "conversation",
  "Jizzy::Schema::Result::Conversation",
  { id => "conversation_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 user

Type: belongs_to

Related object: L<Jizzy::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "Jizzy::Schema::Result::User",
  { id => "user_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 16:53:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1ctGDFDr3YO3YsbAVrF6fQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
