package Jizzy::Controller::Photos;
use Mojo::Base 'Mojolicious::Controller';
require DateTime;

# This action will render a template
sub index {
  my $self = shift;

  # Render template "example/welcome.html.ep" with message
  $self->render(msg => '');
}

sub store {
  my $self = shift;

  $self->stash(photo => {
    image      		=> '',
    offer_options	=> $self->helpers->select_options_from_resultset('Offer'),
  });

  # Render the template
  $self->render;
}

sub edit {
	my $self = shift;

  # Fetch the category id from the parameters
  my $id = $self->param('id');

  # Get information from the database about the offer
  my $result = $self->db->resultset('Photo')->find($id);
  $self->param('offer_id', $result->offer_id)
    unless $self->param('offer_id');

  $self->stash(photo => {
		id						=> $result->id,
    image      		=> $result->image,
    offer_options	=> $self->select_options_from_resultset('Offer'),
  });

  # Render the template
  $self->render;
}

sub create {
	my $self = shift;

	# Grab the request parameters
	my $image = $self->param('image');
	my $offer_id = $self->param('offer_id');

	# Persist the photo
	$self->db->resultset('Photo')->create({
		image			=> $image,
		offer_id	=> $offer_id,

		# Use the username as author name for now
		#author => $self->session('username'),
	});

	$self->flash(photo_saved => 1);
	$self->redirect_to('/photos');
}

sub update {
	my $self = shift;

	# Grab the request parameters
	my $image = $self->param('image');
	my $offer_id = $self->param('offer_id');

	# Persist the photo
	$self->db->resultset('Photo')->find($self->param('id'))->update({
		image			=> $image,
		offer_id		=> $offer_id,

		# Use the username as author name for now
		#author => $self->session('username'),
	});

	$self->flash(photo_saved => 1);
	$self->redirect_to('/photos');
}

sub remove {
	my $self = shift;

	my $photos = $self->db->resultset('Photo');
	$self->app->log->info($self->stash('id'));
	$photos->search({ id => $self->stash('id') })->delete;

	$self->flash(photo_deleted => '1');
	$self->redirect_to('/photos');
}

1;