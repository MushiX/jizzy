package Jizzy::Controller::Categories;
use Mojo::Base 'Mojolicious::Controller';

# This action will render a template
sub index {
  my $self = shift;

  # Render template "example/welcome.html.ep" with message
  $self->render(msg => '');
}

=begin
sub new {
	my $self = shift;

	my $categories = Mojo::Collection->new(['*' => 0]);
	my $cats = $self->db->resultset('Category');
	while (my $cat = $cats->next) {
		push @{$categories}, [$cat->name, $cat->id];
	}
}
=cut

# I using this manual: http://oliverguenther.de/2014/04/applications-with-mojolicious-part-five-dbic-integration-in-mojolicious/
sub store { # new -> this is only "name of function" ? yep [done! rename file, categories#store, and action] WORK!!!!!! ; ))))
  my $self = shift;

  # Fetch the category id from the parameters
  #my $id = $self->param('id');

  # Get information from the database about the category
  # Ok but... What I must use ?
  # There is no point fetching the category here as there isn't one to find
  #my $result = $self->db->resultset('Category')->find($id); # in this place too ? yes, $self refers to the controller(!) here
  #$self->param('parent_id', $result->parent_id)
  #  unless $self->param('parent_id');

  # Add information in the stash for the template (e.g. edit.html.ep)
  # We "restructure" the information to decouple the template from the database
  # You don't *have to*, but I think it is good structure
  my $category_options = $self->helpers->select_options_from_resultset('Category'); # here we can pass in the category options
  #$self->app->log->debug(Mojo::Util::dumper($category_options)); # what output do you get? / How use it logs ? It should output it in the terminal
  # oh! don't call this method "new", it's effectively a reserved method name (for creating objects) / ok I change name to store ;}
  # Sorry that took so long to realise.. my head is in a different problem space
  # no problem :) I have time :)
  # usually, the action for showing the create page is called "create" and the action for
  # storing the new item is called "store" / store reminds me of shop(app store, play store etc...)
  # https://github.com/mojolicious/mojo-pg/blob/master/examples/blog/lib/Blog.pm

  $self->stash(category => { # You can use these to pass in the defaults you want
    #id        => $result->id,
    name      => '',
    position  => 1,
    options   => $category_options, # try this
  });
  #$self is the same data in all $self at this sub edit {}? yes

  # Errors resultset ;P list:
  #ERROR: Undefined subroutine &Jizzy::Controller::Categories::select_options_from_resultset called at
  #       ~.perl5/lib/perl5/Mojolicious/Routes.pm line 127. // solution: $self->helpers->...
  # But you don't get that on edit..?

  #ERROR: Can't use string ("Jizzy::Controller::Categories") as a HASH ref while "strict refs" in use at
  #       ~.perl5/lib/perl5/Mojo/Base.pm line 72. // solution:  my $category_options = self->... <- not work

  # Render the template
  $self->render;
}

sub edit {
	my $self = shift;

  # Fetch the category id from the parameters
  my $id = $self->param('id');

  # Get information from the database about the category
  my $result = $self->db->resultset('Category')->find($id); # in this place too ? yes, $self refers to the controller(!) here
  $self->param('parent_id', $result->parent_id)
    unless $self->param('parent_id');

  # Add information in the stash for the template (e.g. edit.html.ep)
  # We "restructure" the information to decouple the template from the database
  # You don't *have to*, but I think it is good structure
  $self->stash(category => {
    id        => $result->id,
    name      => $result->name,
    position  => $result->position,
    options   => $self->select_options_from_resultset('Category'), # here we can pass in the category options
  });
  #$self is the same data in all $self at this sub edit {}? yes

  # Render the template
  $self->render;
}


sub create {
	my $self = shift;

	# Grab the request parameters
	my $name = $self->param('name');
  my $parent_id = $self->param('parent_id');
	my $position = $self->param('position');

	# Persist the category
	$self->db->resultset('Category')->create({
		name => $name,
    parent_id => $parent_id,
		position => $position,

		# Use the username as author name for now
		#author => $self->session('username'),

		# Published now
		#date_published => DateTime->now->iso8601,
	});

	$self->flash(category_saved => 1);
	$self->redirect_to("/categories");
}

sub update {
	my $self = shift;

	# Grab the request parameters
	my $name = $self->param('name');
  my $parent_id = $self->param('parent_id');
	my $position = $self->param('position');

	# Persist the category
	$self->db->resultset('Category')->find($self->param('id'))->update({
		name => $name,
    parent_id => $parent_id,
		position => $position,

		# Use the username as author name for now
		#author => $self->session('username'),

		# Published now
		#date_published => DateTime->now->iso8601,
	});

	$self->flash(category_saved => 1);
	$self->redirect_to('/categories');
}

sub remove {
	my $self = shift;

	my $categories = $self->db->resultset('Category');
	$self->app->log->info($self->stash('id'));
	$categories->search({ id => $self->stash('id') })->delete;

	$self->flash(category_deleted => '1');
	$self->redirect_to('/categories');
}

1;
