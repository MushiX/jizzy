package Jizzy::Controller::Users;
use Mojo::Base 'Mojolicious::Controller';
require DateTime;

# This action will render a template
sub index {
  my $self = shift;

  # Render template "example/welcome.html.ep" with message
  $self->render(msg => '');
}

sub store {
  my $self = shift;

  $self->stash(user => {
    #id        		=> $result->id,
    name      		=> '',
		surname				=> '',
		email					=> '',
    city_options	=> $self->helpers->select_options_from_resultset('City'),
		address				=> '',
		lang					=> '',
		avatar				=> '',
		password			=> '',
  });

  # Render the template
  $self->render;
}

sub edit {
	my $self = shift;

  # Fetch the category id from the parameters
  my $id = $self->param('id');

  # Get information from the database about the city
  my $result = $self->db->resultset('User')->find($id);
  $self->param('city_id', $result->city_id)
    unless $self->param('city_id');

  $self->stash(user => {
		id						=> $result->id,
    name      		=> $result->name,
		surname				=> $result->surname,
		email					=> $result->email,
    city_options	=> $self->select_options_from_resultset('City'),
		address				=> $result->address,
		lang					=> $result->lang,
		avatar				=> $result->avatar,
		password			=> $result->password,
  });

  # Render the template
  $self->render;
}

sub create {
	my $self = shift;

	# Grab the request parameters
	my $name = $self->param('name');
	my $surname = $self->param('surname');
	my $address = $self->param('address');
	my $email = $self->param('email');
	my $lang = $self->param('lang');
	my $password = $self->param('password');
	my $avatar = $self->param('avatar');
	my $is_admin = $self->param('is_admin');
	my $is_active = $self->param('is_active');
	#my $created_at = $self->param('created_at');
	#my $updated_at = $self->param('updated_at');
	my $city_id = $self->param('city_id');

	# Persist the user
	$self->db->resultset('User')->create({
		name			=> $name,
		surname 	=> $surname,
    address 	=> $address,
    email			=> $email,
		lang			=> $lang,
    password 	=> $password,
    avatar		=> $avatar,
    is_admin	=> $is_admin,
    is_active	=> $is_active,
		city_id		=> $city_id,

		# Use the username as author name for now
		#author => $self->session('username'),

		# Published now
    created_at => DateTime->now->iso8601,
		updated_at => DateTime->now->iso8601,
	});

	$self->flash(user_saved => 1);
	$self->redirect_to('/users');
}

sub update {
	my $self = shift;

	# Grab the request parameters
	my $name = $self->param('name');
	my $surname = $self->param('surname');
	my $address = $self->param('address');
	my $email = $self->param('email');
	my $lang = $self->param('lang');
	my $password = $self->param('password');
	my $avatar = $self->param('avatar');
	my $is_admin = $self->param('is_admin');
	my $is_active = $self->param('is_active');
	#my $created_at = $self->param('created_at');
	#my $updated_at = $self->param('updated_at');
	my $city_id = $self->param('city_id');

	# Persist the user
	$self->db->resultset('User')->find($self->param('id'))->update({
		name			=> $name,
		surname		=> $surname,
    address		=> $address,
    email			=> $email,
		lang			=> $lang,
    password	=> $password,
    avatar		=> $avatar,
    is_admin	=> $is_admin,
    is_active	=> $is_active,
		city_id		=> $city_id,

		# Use the username as author name for now
		#author => $self->session('username'),

		# Published now
		updated_at => DateTime->now->iso8601,
	});

	$self->flash(user_saved => 1);
	$self->redirect_to('/users');
}

sub remove {
	my $self = shift;

	my $users = $self->db->resultset('User');
	$self->app->log->info($self->stash('id'));
	$users->search({ id => $self->stash('id') })->delete;

	$self->flash(user_deleted => '1');
	$self->redirect_to('/users');
}

1;
