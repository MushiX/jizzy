#use utf8;
package Jizzy::Controller::Cities;
use Mojo::Base 'Mojolicious::Controller';

=begin
sub _city_from_stash {
	my $self = shift;
	return $self->db->resultset('City')->find($self->stash('id'));
}
=cut

# This action will render a template
sub index {
  my $self = shift;

  # Render template "example/welcome.html.ep" with message
  $self->render(msg => '');
}

=begin
sub show {
	my $self = shift;
	#my $city = $self->db->resultset('City')->find($self->stash('id'));
	my $city = $self->_city_from_stash;
	#my $user = $self->_user_from_session;

	if (defined $city) {
		$self->render(city => $city);
		#$self->render(post => $post, logged_in => defined($user), user => $user);
	} else {
		#$self->render_not_found;
		$self->render(cities);
	}
}
=cut

sub create {
	my $self = shift;

	# Grab the request parameters
	my $name = $self->param('name');
	my $zip_code = $self->param('zip_code');

	# Persist the city
	$self->db->resultset('City')->create({
		name => $name,
		zip_code => $zip_code,

		# Use the username as author name for now
		#author => $self->session('username'),

		# Published now
		#date_published => DateTime->now->iso8601,
	});

	$self->flash(city_saved => 1);
	$self->redirect_to("/cities");
}

=begin
sub edit {
	my $self = shift;
	#my $city = $self->db->resultset('City')->find($self->stash('id'));
	my $city = $self->_city_from_stash;
	#my $user = $self->_user_from_session;

	if (defined $city) {
		$self->render(city => $city);
		#$self->render(post => $post, logged_in => defined($user), user => $user);
	} else {
		#$self->render_not_found;
		$self->render(cities);
	}
}
=cut

sub update {
	my $self = shift;

	# Grab the request parameters
	my $name = $self->param('name');
	my $zip_code = $self->param('zip_code');

	# Persist the city
	$self->db->resultset('City')->find($self->param('id'))->update({
		name => $name,
		zip_code => $zip_code,

		# Use the username as author name for now
		#author => $self->session('username'),

		# Published now
		#date_published => DateTime->now->iso8601,
	});

	$self->flash(city_saved => 1);
	$self->redirect_to('/cities');
}

sub remove {
	my $self = shift;

	my $cities = $self->db->resultset('City');
	$self->app->log->info($self->stash('id'));
	$cities->search({ id => $self->stash('id') })->delete;

	$self->flash(city_deleted => '1');
	$self->redirect_to('/cities');
}

1;
