package Jizzy::Controller::Offers;
use Mojo::Base 'Mojolicious::Controller';
require DateTime;

# This action will render a template
sub index {
  my $self = shift;

  # Render template "example/welcome.html.ep" with message
  $self->render(msg => '');
}

sub store {
  my $self = shift;

  $self->stash(offer => {
    #id        				=> $result->id,
    title      				=> '',
		content						=> '',
		price							=> '',
    visible						=> '',
		end_at						=> '',
		category_options	=> $self->helpers->select_options_from_resultset('Category'),
		user_options			=> '', #$self->helpers->select_options_from_resultset('User'),
		city_options			=> $self->helpers->select_options_from_resultset('City'),
  });

  # Render the template
  $self->render;
}

sub edit {
	my $self = shift;

  # Fetch the category id from the parameters
  my $id = $self->param('id');

  # Get information from the database about the city
  my $result = $self->db->resultset('Offer')->find($id);
  $self->param('category_id', $result->category_id)
    unless $self->param('category_id');
  $self->param('user_id', $result->user_id)
    unless $self->param('user_id');
  $self->param('city_id', $result->city_id)
    unless $self->param('city_id');

  $self->stash(offer => {
		id						=> $result->id,
    title      		=> $result->title,
		content				=> $result->content,
		price					=> $result->price,
    visible				=> $result->visible,
		end_at				=> $result->end_at,
		category_options		=> $self->select_options_from_resultset('Category'),
		user_options				=> $self->select_options_from_resultset('User'),
		city_options				=> $self->select_options_from_resultset('City'),
  });

  # Render the template
  $self->render;
}

sub create {
	my $self = shift;

	# Grab the request parameters
	my $title = $self->param('title');
	my $content = $self->param('content');
  my $price = $self->param('price');
  my $visible = $self->param('visible');
  #my $visible = f;
  my $end_at = $self->param('end_at');
  my $category_id = $self->param('category_id');
  my $user_id = $self->param('user_id');
  my $city_id = $self->param('city_id');

	# Persist the offer
	$self->db->resultset('Offer')->create({
		title => $title,
		content => $content,
    price => $price,
    visible => $visible,
    end_at => $end_at,
    category_id => $category_id,
    user_id => $user_id,
    city_id => $city_id,

		# Use the username as author name for now
		#author => $self->session('username'),

		# Published now
    created_at => DateTime->now->iso8601,
		updated_at => DateTime->now->iso8601,
	});

	$self->flash(offer_saved => 1);
	$self->redirect_to('/offers');
}

sub update {
	my $self = shift;

	# Grab the request parameters
	my $title = $self->param('title');
	my $content = $self->param('content');
  my $price = $self->param('price');
  my $visible = $self->param('visible');
  my $end_at = $self->param('end_at');
  #my $created_at = $self->param('created_at');
  #my $updated_at = $self->param('updated_at');
  my $category_id = $self->param('category_id');
  my $user_id = $self->param('user_id');
  my $city_id = $self->param('city_id');

	# Persist the offer
	$self->db->resultset('Offer')->find($self->param('id'))->update({
		title => $title,
		content => $content,
    price => $price,
    visible => $visible,
    end_at => $end_at,
    category_id => $category_id,
    user_id => $user_id,
    city_id => $city_id,

		# Use the username as author name for now
		#author => $self->session('username'),

		# Published now
		updated_at => DateTime->now->iso8601,
	});

	$self->flash(offer_saved => 1);
	$self->redirect_to('/offers');
}

sub remove {
	my $self = shift;

	my $offers = $self->db->resultset('Offer');
	$self->app->log->info($self->stash('id'));
	$offers->search({ id => $self->stash('id') })->delete;

	$self->flash(offer_deleted => '1');
	$self->redirect_to('/offers');
}

1;